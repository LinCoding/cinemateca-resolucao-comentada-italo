package br.uscsal.testequalidade20162.radio.business;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;

import br.uscsal.testequalidade20162.radio.domain.Album;
import br.uscsal.testequalidade20162.radio.domain.Musica;
import br.uscsal.testequalidade20162.radio.exceptions.RegistroDuplicadoException;
import br.uscsal.testequalidade20162.radio.exceptions.RegistroNaoEncontradoException;
import br.uscsal.testequalidade20162.radio.persistence.AlbumDao;
import br.uscsal.testequalidade20162.radio.persistence.MusicaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RadioBO.class)

public class RadioBOIntegradoTest {

	// Método a ser testado: public static void adicionarMusica(String
	// tituloAlbum, String nomeMusica) throws
	// RegistroNaoEncontradoException,
	// Verificar se a inclusão de uma música duplicada em um album retorna a
	// exceção adequada.
	// LEMBRE-SE DE QUE ESTE TESTE DEVE SER INTEGRADO!!!

	List<Album> albunsTeste;
	List<Musica> musicasAddTeste;
	Album albumTeste;
	Musica musicaAddAlbum;

	String nomeAlbumTeste = "Teste";
	String nomeMusicaTeste = "teste";

	@Test(expected = RegistroDuplicadoException.class)
	public void adicionarAtorNascidoAntesLancamentoFilme()
			throws RegistroNaoEncontradoException, RegistroDuplicadoException {

		RadioBO.incluirAlbum(nomeAlbumTeste, null, null);
		RadioBO.incluirMusica(nomeMusicaTeste, null, null);

		albunsTeste = WhiteboxImpl.getInternalState(AlbumDao.class, "albuns");
		musicasAddTeste = WhiteboxImpl.getInternalState(MusicaDao.class, "musicas");

		albumTeste = albunsTeste.get(0);
		musicaAddAlbum = musicasAddTeste.get(0);

		albumTeste.adicionarMusica(musicaAddAlbum);

		RadioBO.adicionarMusica(nomeAlbumTeste, nomeMusicaTeste);

	}
}
