package br.uscsal.testequalidade20162.radio.tui;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RadioTui.class })

public class RadioTuiUnitarioTest {

	// Método a ser testado: private static Integer obterTexto(String mensagem)
	// Verificar o processo de entrada e saída para obtenção de um texto.
	// Obs: fazer o mock do System.in e do System.out.

	@Test
	public void verificarObterTexto() throws Exception {
		// Mockar o Scanner
		Scanner powerScanner = PowerMockito.mock(Scanner.class);
		WhiteboxImpl.setInternalState(RadioTui.class, "sc", powerScanner);

		// Mockar e ensinar printStream
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

		// Ensinar a quando o nextLine for chamado retornar Obter Texto
		PowerMockito.doReturn("Obter Texto").when(powerScanner).nextLine();

		// Chamar o metodo
		WhiteboxImpl.invokeMethod(RadioTui.class, "obterTexto", "teste");

		// Verificar se a saida do metodo chamado foi um "teste"
		Mockito.verify(printStreamMock).println("teste");

	}
}
