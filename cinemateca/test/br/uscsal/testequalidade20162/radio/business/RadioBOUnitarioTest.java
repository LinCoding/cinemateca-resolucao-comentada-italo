package br.uscsal.testequalidade20162.radio.business;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;

import br.uscsal.testequalidade20162.radio.domain.Album;
import br.uscsal.testequalidade20162.radio.domain.Musica;
import br.uscsal.testequalidade20162.radio.exceptions.RegistroDuplicadoException;
import br.uscsal.testequalidade20162.radio.persistence.AlbumDao;
import br.uscsal.testequalidade20162.radio.persistence.MusicaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RadioBO.class)

public class RadioBOUnitarioTest {

	// MÃ©todo a ser testado: public static void adicionarMusica(String
	// tituloAlbum, String nomeMusica) throws
	// RegistroNaoEncontradoException,
	// Verificar se a inclusÃ£o de uma mÃºsica duplicada em um album retorna a
	// exceÃ§Ã£o adequada.
	// LEMBRE-SE DE QUE ESTE TESTE DEVE SER UNITÃ�RIO!!!

	List<Musica> musicasTeste;
	List<Musica> listaMusicaAlbum;
	List<Album> albumTeste;

	String nomeAlbumTeste = "The Getaway";
	String nomeMusicaTeste = "Dark Necessities";

	@Before
	public void setUp() {

		Musica musicaAtual = new Musica(nomeMusicaTeste, 4, "Lin");

		// Listas Album e Musicas Teste receberam a lista privada das classes
		// DAO.
		albumTeste = WhiteboxImpl.getInternalState(AlbumDao.class, "albuns");
		musicasTeste = WhiteboxImpl.getInternalState(MusicaDao.class, "musicas");

		// Adicionei as listas que recebi Novas instancias de Album e musica
		albumTeste.add(new Album(nomeAlbumTeste, null, null));
		musicasTeste.add(musicaAtual);

		// Armazenei a instancia de album que acabei de criar dentro de uma
		// variavel
		// Assim eu posso ter acesso a List de Musicas que existem dentro de
		// Album com meu WhiteBox
		Album albumAtual = albumTeste.get(0);
		listaMusicaAlbum = WhiteboxImpl.getInternalState(albumAtual, "musicas");
		// Adicionei a mesma instacia de musica ( List do Musicas DAO) na lista
		// de musicas do Album para pegar a Exception.
		listaMusicaAlbum.add(musicaAtual);

		// Apos isso tudo settei os valores que havia modificado para poder
		// gerar um caso de teste.
		// PS: Uso o whiteBox pois todo encapsulamento deles é privado.
		WhiteboxImpl.setInternalState(AlbumDao.class, "albuns", albumTeste);
		WhiteboxImpl.setInternalState(MusicaDao.class, "musicas", musicasTeste);
		WhiteboxImpl.setInternalState(albumAtual, "musicas", listaMusicaAlbum);

	}

	@Test(expected = RegistroDuplicadoException.class)
	public void adicionarAtorNascidoAntesLancamentoFilme() throws Exception {
		// Basta voce adicionar um InvokeMethod com a passagem de parametros
		// correta, o nome do album que criamos no Before
		// com o nome da musica que instaciamos

		// PS: é necessário colocar o expected pois voce espera que o teste te
		// retorne uma excpetion do tipo Registro Duplicado
		RadioBO.adicionarMusica(nomeAlbumTeste, nomeMusicaTeste);
	}
}
