package br.uscsal.testequalidade20162.radio.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;

import br.uscsal.testequalidade20162.radio.exceptions.RegistroDuplicadoException;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Album.class)
public class AlbumUnitarioTest {

	// Método a ser testado: private void validarMusica(Musica musica) throws
	// RegistroDuplicadoException {
	// Verificar que é levantada a exceção adequada quando uma música já
	// existente em um album é validada.

	Album instancia1;
	Musica musica;

	@Before
	public void setUp() {

		instancia1 = new Album(null, null, null);
		musica = new Musica("teste", 4, "billie teste");
		instancia1.musicas.add(musica);

	}

	@Test(expected = RegistroDuplicadoException.class)
	public void validarAtorNascidoDataPosteriorLancamentoFilme() throws Exception {

		WhiteboxImpl.invokeMethod(instancia1, "adicionarMusica", musica);

	}

}
